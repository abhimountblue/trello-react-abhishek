import { deleteCheckItems, updateCheckItem } from "../services/api"
import { Button, Checkbox, Typography } from "@mui/material"
import { Delete } from "@mui/icons-material"
import { useToast } from "../context/ToastContext"

function CheckItem({
  CheckItemData,
  idCard,
  deleteFunction,
  updateItemFunction,
}) {
  const checkedItemValue = CheckItemData.state === "complete" ? true : false
  const { setToastStatus, setOpenError } = useToast()

  async function checkedValueHandler() {
    try {
      await updateCheckItem(
        idCard,
        CheckItemData.id,
        checkedItemValue === true ? "incomplete" : "complete"
      )
      updateItemFunction(
        CheckItemData.id,
        checkedItemValue === true ? "incomplete" : "complete"
      )
      setToastStatus({
        message: "CheckList Item status updated",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  async function deleteItemHandler(checkListId, itemId) {
    try {
      await deleteCheckItems(checkListId, itemId)
      deleteFunction(itemId)
      setToastStatus({
        message: "CheckList Item Deleted",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }
  return (
    <>
      <Typography
        component={"div"}
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography
          component={"div"}
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Checkbox checked={checkedItemValue} onChange={checkedValueHandler} />
          <Typography
            sx={{
              textDecoration: checkedItemValue ? "line-through" : "none",
            }}
          >
            {" "}
            {CheckItemData.name}
          </Typography>
        </Typography>

        <Button
          onClick={() =>
            deleteItemHandler(CheckItemData.idChecklist, CheckItemData.id)
          }
          sx={{
            ":hover": {
              color: "red",
            },
          }}
        >
          <Delete />
        </Button>
      </Typography>
    </>
  )
}

export default CheckItem
