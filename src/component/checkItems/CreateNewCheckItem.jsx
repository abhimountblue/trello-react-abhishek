import { useState } from "react"
import { addCheckItem } from "../services/api"
import { Button, TextField, Typography } from "@mui/material"
import ClearRoundedIcon from "@mui/icons-material/ClearRounded"
import { useToast } from "../context/ToastContext"

function CreateNewCheckItem({ checkListId, addFunction }) {
  const [checkItemName, setCheckItemName] = useState("")
  const [clickedOnNewItem, setClickedOnNewItem] = useState(false)
  const { setToastStatus, setOpenError } = useToast()

  async function addNewCheckListItemHandler() {
    try {
      if (checkItemName === "") {
        return
      }
      const newCheckList = await addCheckItem(checkItemName, checkListId)
      setCheckItemName("")
      addFunction(newCheckList, checkListId)
      setToastStatus({
        message: "Added new Item",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  return (
    <Typography marginLeft={4} component={"div"}>
      {clickedOnNewItem ? (
        <>
          <TextField
            value={checkItemName}
            placeholder="Enter a title for this item..."
            sx={{
              background: "#fff",
              borderRadius: "10px",
              marginBottom: 2,
            }}
            onChange={(event) => {
              setCheckItemName(event.target.value)
            }}
          />
          <Typography component={"div"}>
            <Button
              variant="contained"
              onClick={() => addNewCheckListItemHandler()}
            >
              + Add
            </Button>
            <Button
              sx={{
                ":hover": {
                  color: "red",
                },
              }}
              onClick={() => setClickedOnNewItem(false)}
            >
              <ClearRoundedIcon />
            </Button>
          </Typography>
        </>
      ) : (
        <Typography
          component={"h6"}
          sx={{
            cursor: "pointer",
            margin: 2,
          }}
          onClick={() => setClickedOnNewItem(true)}
        >
          + Add an item
        </Typography>
      )}
    </Typography>
  )
}

export default CreateNewCheckItem
