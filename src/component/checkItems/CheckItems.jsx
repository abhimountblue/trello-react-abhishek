import ProgressBar from "../ProgressBar"
import { progressBarStatusHandler } from "../utils/common"
import CheckItem from "./CheckItem"
import CreateNewCheckItem from "./CreateNewCheckItem"

function CheckItems({ updateFunction, checkListData }) {
  const { checkItems, idCard, id } = checkListData

  function deleteCheckedItem(itemId) {
    const afterDeletedItem = checkItems.filter((element) => {
      return element.id !== itemId
    })
    updateFunction(checkListData.id, {
      ...checkListData,
      ["checkItems"]: afterDeletedItem,
    })
  }

  function addCheckItemHandler(item ,checkListId) {
    const newAddedItem = [...checkItems, item]
    updateFunction(checkListId, {
      ...checkListData,
      ["checkItems"]: newAddedItem,
    })
  }

  function updateCheckListItem(itemId, value) {
    const afterUpdate = checkItems.map((element) => {
      if (element.id === itemId) {
        element.state = value
      }
      return element
    })
    updateFunction(checkListData.id, {
      ...checkListData,
      ["checkItems"]: afterUpdate,
    })
  }

  const progressedValue = progressBarStatusHandler(checkItems)

  return (
    <>
      {checkItems.length !== 0 ? <ProgressBar value={progressedValue} /> : null}
      {checkItems.map((element) => {
        return (
          <CheckItem
            key={element.id}
            CheckItemData={element}
            idCard={idCard}
            deleteFunction={deleteCheckedItem}
            updateItemFunction={updateCheckListItem}
          />
        )
      })}
      <CreateNewCheckItem checkListId={id} addFunction={addCheckItemHandler} />
    </>
  )
}

export default CheckItems
