import axios from "axios"
// import config from '../../../config'

const API_KEY = "92bb15c6fff9e73ee27347ca4a002c0f"
const TOKEN = "ATTAf9eec9a0d22ba72f22a8bf57e90c76e9b3d43b9089c34b9ea38e3813c13c0b6e04A2FA89"
const BASE_URL = "https://api.trello.com/1/"

export const getAllBoards = async () => {
    const boardsUrl = `${BASE_URL}members/me/boards?key=${API_KEY}&token=${TOKEN}`
    const allBoard = await axios.get(boardsUrl)
    return allBoard.data
}

export const createNewBoard = async (name) => {
    const addBoardUrl = `${BASE_URL}boards/?name=${name}&key=${API_KEY}&token=${TOKEN}`
    const createdBoard = await axios.post(addBoardUrl)
    return createdBoard.data
}

export const getAllLists = async (boardId) => {
    const listsUrl = `${BASE_URL}boards/${boardId}/lists?key=${API_KEY}&token=${TOKEN}`
    const allLists = await axios.get(listsUrl)
    return allLists.data
}

export const addNewList = async (name, id) => {
    const addListUrl = `${BASE_URL}boards/${id}/lists?name=${name}&key=${API_KEY}&token=${TOKEN}`
    const createdList = await axios.post(addListUrl)
    return createdList.data
}

export const getAllCards = async (listId) => {
    const cardsUrl = `${BASE_URL}lists/${listId}/cards?key=${API_KEY}&token=${TOKEN}`
    const allCards = await axios.get(cardsUrl)
    return allCards.data
}


export const addNewCard = async (name, listId) => {
    const addCardUrl = `${BASE_URL}cards?name=${name}&idList=${listId}&key=${API_KEY}&token=${TOKEN}`
    const createdCard = await axios.post(addCardUrl)
    return createdCard.data
}

export const deleteList = async (id) => {
    const deleteListUrl = `${BASE_URL}lists/${id}?closed=true&key=${API_KEY}&token=${TOKEN}`
    const deletedList = await axios.put(deleteListUrl)
    return deletedList.data
}

export const deleteCard = async (id) => {
    const deleteCardUrl = `${BASE_URL}cards/${id}?key=${API_KEY}&token=${TOKEN}`
    const deletedCard = await axios.delete(deleteCardUrl)
    return deletedCard.data
}

export const addCheckList = async (name, cardId) => {
    const checkListsUrl = `${BASE_URL}checklists?name=${name}&idCard=${cardId}&key=${API_KEY}&token=${TOKEN}`
    const createdCheckList = await axios.post(checkListsUrl)
    return createdCheckList.data
}

export const getCheckLists = async (cardId) => {
    const checkListUrl = `${BASE_URL}cards/${cardId}/checklists?key=${API_KEY}&token=${TOKEN}`
    const allCheckList = await axios.get(checkListUrl)
    return allCheckList.data
}

export const deleteCheckList = async (CardId, CheckListId) => {
    const deleteCheckListUrl = `${BASE_URL}cards/${CardId}/checklists/${CheckListId}?key=${API_KEY}&token=${TOKEN}`
    const deletedCheckList = await axios.delete(deleteCheckListUrl)
    return deletedCheckList.data
}

export const addCheckItem = async (name, checkListId) => {
    const addCheckItemUrl = `${BASE_URL}checklists/${checkListId}/checkItems?name=${name}&key=${API_KEY}&token=${TOKEN}`
    const createdItem = await axios.post(addCheckItemUrl)
    return createdItem.data
}

export const getCheckItems = (checkListId) => {

    const checkItemUrl = `${BASE_URL}checklists/${checkListId}/checkItems?key=${API_KEY}&token=${TOKEN}`
    return axios.get(checkItemUrl).then((response) => response.data)
}

export const deleteCheckItems = async (checkListId, checkItemId) => {
    const deleteCheckItemUrl = `${BASE_URL}checklists/${checkListId}/checkItems/${checkItemId}?key=${API_KEY}&token=${TOKEN}`
    const itemDeleted = await axios.delete(deleteCheckItemUrl)
    return itemDeleted.data
}

export const updateCheckItem = async (idCard, idCheckItem, status) => {
    const updateCheckItemUrl = `${BASE_URL}cards/${idCard}/checkItem/${idCheckItem}?key=${API_KEY}&token=${TOKEN}`
    const data = {
        state: status
    }
    const currentStatus = await axios.put(updateCheckItemUrl, data)
    return currentStatus.data
}
