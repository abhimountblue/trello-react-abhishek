import { useEffect, useState } from "react"
import { getCheckLists } from "../services/api"
import CheckList from "./CheckList"
import CreateNewCheckList from "./CreateNewCheckList"
import { addItemToState, deleteItemFromState } from "../utils/common"
import { useToast } from "../context/ToastContext"

function CheckListContrainer({ cardId }) {
  const [checkLists, setCheckLists] = useState([])
  const { setToastStatus, setOpenError } = useToast()

  useEffect(() => {
    getAllCheckLists(cardId)
  }, [])

  async function getAllCheckLists(cardId) {
    try {
      const allCheckList = await getCheckLists(cardId)
      setCheckLists(allCheckList)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  function addCheckListHandler(item) {
    addItemToState(setCheckLists, item)
  }

  function updateCheckList(id, value) {
    const afterUpdate = checkLists.map((element) => {
      if (element.id === id) {
        return value
      }
      return element
    })
    setCheckLists([...afterUpdate])
  }

  function deleteCheckListHandler(id) {
    deleteItemFromState(checkLists, setCheckLists, id)
  }
  return (
    <>
      {checkLists.map((element) => {
        return (
          <CheckList
            key={element.id}
            checkListData={element}
            deleteFunction={deleteCheckListHandler}
            updateFunction={updateCheckList}
          />
        )
      })}
      {<CreateNewCheckList cardId={cardId} addFunction={addCheckListHandler} />}
    </>
  )
}

export default CheckListContrainer
