import { useState } from "react"
import { addCheckList } from "../services/api"
import { Button, Card, TextField, Typography } from "@mui/material"
import ClearRoundedIcon from "@mui/icons-material/ClearRounded"
import { useToast } from "../context/ToastContext"

function CreateNewCheckList({ cardId, addFunction }) {
  const [checkListName, setCheckListName] = useState("")
  const [clickedOnNewList, setClickedOnNewList] = useState(false)
  const { setToastStatus, setOpenError } = useToast()

  async function addNewCheckListHandler() {
    try {
      if (checkListName === "") {
        return
      }
      const newCheckList = await addCheckList(checkListName, cardId)
      setCheckListName("")
      addFunction(newCheckList)
      setToastStatus({
        message: "New CheckList Added",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }
  return (
    <Card
      variant="outlined"
      sx={{
        background: "none",
        border: "none",
        display: "flex",
        flexDirection: "column",
      }}
    >
      {clickedOnNewList ? (
        <>
          <TextField
            variant="filled"
            sx={{
              background: "#fff",
              borderRadius: "10px",
              marginBottom: 2,
            }}
            value={checkListName}
            placeholder="Enter a title for this checklist..."
            onChange={(event) => {
              setCheckListName(event.target.value)
            }}
          />
          <Typography component={"div"}>
            <Button
              variant="contained"
              onClick={() => addNewCheckListHandler()}
            >
              + Add
            </Button>
            <Button
              sx={{
                ":hover": {
                  color: "red",
                },
              }}
              onClick={() => setClickedOnNewList(false)}
            >
              <ClearRoundedIcon />
            </Button>
          </Typography>
        </>
      ) : (
        <>
          <Typography
            component={"h6"}
            sx={{
              cursor: "pointer",
              margin: 2,
              color: "#fff",
            }}
            onClick={() => setClickedOnNewList(true)}
          >
            + Add a CheckList
          </Typography>
        </>
      )}
    </Card>
  )
}

export default CreateNewCheckList
