import { deleteCheckList } from "../services/api"
import { Button, Typography } from "@mui/material"
import { Delete } from "@mui/icons-material"
import CheckItems from "../checkItems/CheckItems"
import { useToast } from "../context/ToastContext"

function CheckList({ checkListData, deleteFunction, updateFunction }) {
  const { idCard, id } = checkListData
  const { setToastStatus, setOpenError } = useToast()

  async function deleteCheckListHandler(idCard, id) {
    try {
      await deleteCheckList(idCard, id)
      deleteFunction(id)
      setToastStatus({
        message: "CheckList Deleted",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  return (
    <>
      <Typography component={"h1"}>
        {checkListData.name}{" "}
        <Button
          onClick={() => deleteCheckListHandler(idCard, id)}
          sx={{
            ":hover": {
              color: "red",
            },
          }}
        >
          <Delete />
        </Button>
      </Typography>
      <CheckItems
        updateFunction={updateFunction}
        checkListData={checkListData}
      />
    </>
  )
}

export default CheckList
