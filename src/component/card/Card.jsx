import React, { useState } from "react"
import { Modal } from "@mui/material"
import { Box } from "@mui/material"
import { Typography } from "@mui/material"
import { Button } from "@mui/material"
import CheckListContrainer from "../checkList/CheckListContrainer"
import { Delete } from "@mui/icons-material"

function Card({ cardData, deleteFunction }) {
  const [open, setOpen] = useState(false)

  function handleOpen() {
    setOpen(true)
  }
  function handleClose() {
    setOpen(false)
  }

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "60vw",
    height: "80vh",
    bgcolor: "#323940",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4,
    color: "#fff",
    overflowY: "auto",
  }

  return (
    <>
      <Typography
        component="div"
        sx={{
          display: "flex",
          justifyContent: "space-between",
          width: "100%",
          cursor: "pointer",
          background: "#323A40",
          borderRadius: "10px",
          alignItems: "center",
        }}
      >
        <Typography
          variant="h7"
          sx={{
            color: "#fff",
            marginLeft: 2,
            width: "100%",
          }}
          onClick={handleOpen}
        >
          {cardData.name}
          {"  "}{" "}
        </Typography>
        <Button
          onClick={() => deleteFunction(cardData.id)}
          sx={{
            ":hover": {
              color: "red",
            },
          }}
        >
          <Delete />
        </Button>
      </Typography>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {cardData.name}
          </Typography>
          <CheckListContrainer cardId={cardData.id} />
        </Box>
      </Modal>
    </>
  )
}

export default Card
