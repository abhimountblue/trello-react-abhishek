import { useState } from "react"
import { addNewCard } from "../services/api"
import { Button, TextField, Typography } from "@mui/material"
import ClearRoundedIcon from "@mui/icons-material/ClearRounded"
import { useToast } from "../context/ToastContext"

function CreateNewCard({ listId, addFunction }) {
  const [cardName, setCardName] = useState("")
  const [clickedOnNewCard, setClickedOnNewCard] = useState(false)
  const { setToastStatus, setOpenError } = useToast()

  async function addNewCardHandler() {
    try {
      if (cardName === "") {
        return
      }
      const newCard = await addNewCard(cardName, listId)
      setCardName("")
      addFunction(newCard)
      setClickedOnNewCard(false)
      setToastStatus({
        message: "New Card Added",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }
  return (
    <>
      {clickedOnNewCard ? (
        <>
          {" "}
          <TextField
            sx={{
              background: "#fff",
              borderRadius: "10px",
              marginBottom: 2,
            }}
            variant="outlined"
            value={cardName}
            placeholder="Enter a title for this card..."
            onChange={(event) => {
              setCardName(event.target.value)
            }}
          />
          <Typography component={"div"}>
            <Button onClick={() => addNewCardHandler()}>Create card</Button>
            <Button
              sx={{
                ":hover": {
                  color: "red",
                },
              }}
              onClick={() => setClickedOnNewCard(false)}
            >
              <ClearRoundedIcon />
            </Button>
          </Typography>
        </>
      ) : (
        <Typography variant="h6" onClick={() => setClickedOnNewCard(true)}>
          + Add a card
        </Typography>
      )}
    </>
  )
}

export default CreateNewCard
