import { useEffect, useState } from "react"
import { deleteCard, getAllCards } from "../services/api"
import Card from "./Card"
import CreateNewCard from "./CreateNewCard"
import { addItemToState, deleteItemFromState } from "../utils/common"
import { useToast } from "../context/ToastContext"

function Cards({ listId }) {
  const [cards, setCards] = useState([])
  const { setToastStatus, setOpenError } = useToast()

  useEffect(() => {
    getCards(listId)
  }, [])

  async function getCards(id) {
    try {
      const allCard = await getAllCards(id)
      setCards(allCard)
    } catch (error) {
      console.log(error.message)
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  async function deleteListHadler(id) {
    try {
      await deleteCard(id)
      deleteItemFromState(cards, setCards, id)
      setToastStatus({
        message: "Card Deleted",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  function addListHandler(item) {
    addItemToState(setCards, item)
  }

  return (
    <>
      {cards.map((element) => {
        return (
          <Card
            key={element.id}
            cardData={element}
            deleteFunction={deleteListHadler}
          />
        )
      })}

      {<CreateNewCard listId={listId} addFunction={addListHandler} />}
    </>
  )
}

export default Cards
