import { createContext, useContext, useState } from "react"
const ToastContext = createContext()

export function useToast() {
  return useContext(ToastContext)
}

export function ToastProvider({ children }) {
  const [toastStatus, setToastStatus] = useState({
    type: "error",
    message: "",
  })
  const [openError, setOpenError] = useState(false)
  const handleCloseError = (event, reason) => {
    if (reason === "clickaway") {
      return
    }
    setOpenError(false)
  }

  return (
    <ToastContext.Provider
      value={{
        openError,
        toastStatus,
        handleCloseError,
        setToastStatus,
        setOpenError,
      }}
    >
      {children}
    </ToastContext.Provider>
  )
}
