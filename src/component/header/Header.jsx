import { AppBar, Toolbar, Typography, Button, Box } from "@mui/material"
import { useLocation, useNavigate } from "react-router-dom"
import logo from "../../assets/favicon-icon.png"

const Header = () => {
  const navigate = useNavigate()
  const urlData = useLocation()

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography
          variant="h6"
          display={"flex"}
          sx={{ flexGrow: 1, alignItems: "center" }}
        >
          <Box component="img" sx={{ height: 30 }} alt="Logo" src={logo} />
          &nbsp; &nbsp; Trello
        </Typography>
        {urlData.pathname !== "/" ? (
          <Button color="inherit" onClick={() => navigate("/")}>
            Boards
          </Button>
        ) : null}
      </Toolbar>
    </AppBar>
  )
}

export default Header
