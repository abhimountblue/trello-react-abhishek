import {
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
} from "@mui/material"
import Cards from "../card/Cards"
import { Delete } from "@mui/icons-material"

function List({ listData, deleteFunction }) {
  return (
    <Card
      sx={{
        minWidth: 275,
        backgroundColor: "#088395",
        borderRadius: "10px",
        height: "fit-content",
        margin: 2,
      }}
    >
      <CardContent>
        <Typography
          variant="h5"
          component="div"
          sx={{
            display: "flex",
            justifyContent: "space-between",
            color: "#b6c2cf",
            fontSize: "medium",
            fontWeight: "bold",
          }}
        >
          
          {listData.name}
          <Button onClick={() => deleteFunction(listData.id)} sx={{
          ':hover':{
            color: 'red'
          }
        }}>
            <Delete />
          </Button>
        </Typography>
      </CardContent>
      <CardActions sx={{ display: "flex", flexDirection: "column" }}>
        <Cards listId={listData.id} />
      </CardActions>
    </Card>
  )
}

export default List
