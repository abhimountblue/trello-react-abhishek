import { useEffect, useState } from "react"
import { deleteList, getAllLists } from "../services/api"
import { useParams } from "react-router-dom"
import List from "./List"
import CreateNewList from "./CreateNewList"
import { addItemToState, deleteItemFromState } from "../utils/common"
import CircularProgress from "@mui/material/CircularProgress"
import { Container, Paper } from "@mui/material"
import { useToast } from "../context/ToastContext"

function Lists() {
  const { boardId } = useParams()
  const [lists, setLists] = useState([])
  const [loader, setLoader] = useState(true)
  const { setToastStatus, setOpenError } = useToast()

  // Using UseEffect to get all board lists and list contains only list name , list id and board id
  useEffect(() => {
    getLists(boardId)
  }, [boardId])

  async function deleteListHadler(id) {
    try {
      await deleteList(id)
      deleteItemFromState(lists, setLists, id)
      setToastStatus({
        message: "List Deleted",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  function addListHandler(item) {
    addItemToState(setLists, item)
  }

  async function getLists(id) {
    try {
      const allList = await getAllLists(id)
      setLists(allList)
      setLoader(false)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
      setLoader(false)
    }
  }
  return (
    <Paper
      sx={{
        display: "flex",
        overflowX: "auto",
        minHeight: "95vh",
        background: "#b1b4b9",
      }}
    >
      {loader ? (
        <CircularProgress
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        />
      ) : (
        <>
          <Container
            sx={{
              display: "flex",
            }}
          >
            {lists.map((element) => {
              return (
                <List
                  key={element.id}
                  listData={element}
                  deleteFunction={deleteListHadler}
                />
              )
            })}
            <CreateNewList boardId={boardId} addFunction={addListHandler} />
          </Container>
        </>
      )}
    </Paper>
  )
}

export default Lists
