import { useState } from "react"
import { addNewList } from "../services/api"
import { Button, Card, TextField, Typography } from "@mui/material"
import ClearRoundedIcon from "@mui/icons-material/ClearRounded"
import { useToast } from "../context/ToastContext"

function CreateNewList({ boardId, addFunction }) {
  const [listName, setListName] = useState("")
  const [clickedOnNewList, setClickedOnNewList] = useState(false)
  const { setToastStatus, setOpenError } = useToast()

  async function addNewListHandler() {
    try {
      if (listName === "") {
        return
      }
      const newList = await addNewList(listName, boardId)
      setListName("")
      addFunction(newList)
      setClickedOnNewList(false)
      setToastStatus({
        message: "New List Added",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }

  return (
    <Card
      sx={{
        minWidth: 275,
        backgroundColor: "#088395",
        borderRadius: "10px",
        height: "fit-content",
        margin: 2,
        padding: 2,
      }}
    >
      {clickedOnNewList ? (
        <>
          <TextField
            sx={{
              background: "#fff",
              borderRadius: "10px",
              marginBottom: 2,
            }}
            variant="outlined"
            value={listName}
            placeholder="Enter a title for this list..."
            onChange={(event) => {
              setListName(event.target.value)
            }}
          />
          <Typography component={"div"}>
            <Button variant="contained" onClick={() => addNewListHandler()}>
              + Add list
            </Button>
            <Button
              sx={{
                ":hover": {
                  color: "red",
                },
              }}
              onClick={() => setClickedOnNewList(false)}
            >
              <ClearRoundedIcon />
            </Button>
          </Typography>
        </>
      ) : (
        <Typography variant="h6" onClick={() => setClickedOnNewList(true)}>
          + Add another list
        </Typography>
      )}
    </Card>
  )
}

export default CreateNewList
