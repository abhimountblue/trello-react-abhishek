import { Card, CardContent, Typography } from "@mui/material"
import { Link } from "react-router-dom"

function Board({ board }) {
  return (
    <Link to={`/${board.id}`}>
      <Card
        sx={{
          minWidth: 275,
          backgroundColor: "#088395",
          borderRadius: "10px",
          margin: 2,
          height: "10rem",
          width: "15rem",
          backgroundImage: board.prefs.backgroundImage
            ? `url(${board.prefs.backgroundImage})`
            : `${board.prefs.backgroundColor}`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "100% 100%",
        }}
      >
        <CardContent>
          <Typography
            variant="h5"
            component="div"
            sx={{
              color: "#fff",
              fontWeight: "bold",
            }}
          >
            {board.name}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  )
}

export default Board
