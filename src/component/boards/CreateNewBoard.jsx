import { useState } from "react"
import { createNewBoard } from "../services/api"
import { Button, Card, TextField, Typography } from "@mui/material"
import { useToast } from "../context/ToastContext"

function CreateNewBoard({ addFunction }) {
  const [boardName, setBoardName] = useState("")
  const [clickedOnNewBoard, setClickedOnNewBoard] = useState(false)
  const { setToastStatus, setOpenError } = useToast()

  async function createBoardHandler() {
    try {
      if (boardName === "") {
        return
      }
      const createdBoard = await createNewBoard(boardName)
      addFunction(createdBoard)
      setBoardName("")
      setToastStatus({
        message: "Board Created",
        type: "success",
      })
      setOpenError(true)
    } catch (error) {
      setToastStatus({
        message: error.message,
        type: "error",
      })
      setOpenError(true)
    }
  }
  return (
    <Card
      sx={{
        minWidth: 275,
        backgroundColor: "#088395",
        borderRadius: "10px",
        height: "10rem",
        minHeight: "fit-content",
        width: "15rem",
        margin: 2,
      }}
    >
      {clickedOnNewBoard ? (
        <Card
          variant="outlined"
          sx={{
            background: "none",
            padding: 2,
            border: "none",
          }}
        >
          <TextField
            sx={{
              paddingBottom: 2,
            }}
            variant="outlined"
            value={boardName}
            placeholder="Enter a title for this Board..."
            onChange={(event) => {
              setBoardName(event.target.value)
            }}
          />
          <Button variant="contained" onClick={() => createBoardHandler()}>
            Create Board
          </Button>
        </Card>
      ) : (
        <Typography
          variant="h6"
          padding={2}
          onClick={() => setClickedOnNewBoard(true)}
        >
          Create New Board
        </Typography>
      )}
    </Card>
  )
}

export default CreateNewBoard
