import React from "react"
import Board from "./Board"
import { Typography } from "@mui/material"

function Boards({ allBoards }) {
  return (
    <Typography
      component={"div"}
      sx={{
        display: "flex",
      }}
    >
      {allBoards.map((element) => {
        return <Board key={element.id} board={element} />
      })}
    </Typography>
  )
}

export default Boards
