import { useEffect, useState } from "react"
import Paper from "@mui/material/Paper"
import Typography from "@mui/material/Typography"
import Boards from "../boards/Boards"
import CreateNewBoard from "../boards/CreateNewBoard"
import { addItemToState } from "../utils/common"
import { getAllBoards } from "../services/api"
import Container from "@mui/material/Container"
import CircularProgress from "@mui/material/CircularProgress"
import { useToast } from "../context/ToastContext"

function Home() {
  const [allBoards, setAllBoards] = useState([])
  const [loader, setLoader] = useState(true)
  const { setToastStatus, setOpenError } = useToast()

  // Using useEffect to get all boards and its name, image, etc.
  useEffect(() => {
    fetchBoards()
  }, [])

  async function fetchBoards() {
    try {
      const allBoardsData = await getAllBoards()
      setAllBoards(allBoardsData)
      setLoader(false)
    } catch (error) {
      console.log(error)
      setToastStatus({
        type: "error",
        message: error.message,
      })
      setOpenError(true)
      setLoader(false)
    }
  }

  function addBoardHandler(item) {
    addItemToState(setAllBoards, item)
  }

  return (
    <Paper
      style={{
        minHeight: "95vh",
        padding: "16px",
        overflowX: "auto",
        position: "relative",
      }}
    >
      {loader ? (
        <CircularProgress
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
          }}
        />
      ) : (
        <>
          <Typography variant="h4" textAlign="center">
            Your Boards
          </Typography>
          <Container
            sx={{
              display: "flex",
            }}
          >
            <Boards allBoards={allBoards} />
            <CreateNewBoard addFunction={addBoardHandler} />
          </Container>
        </>
      )}
    </Paper>
  )
}

export default Home
