export function deleteItemFromState(state, setState, itemId) {
    const afterDeleteItem = state.filter((element) => {
        return element.id !== itemId
    })
    setState([...afterDeleteItem])
}

export function addItemToState(setState, item) {
    setState((prev) => {
        return [...prev, item]
    })
}

export function progressBarStatusHandler(state = []) {
    const stateLength = state.length
    const completedTask = state.reduce((acc, element) => {
        if (element.state === "complete") {
            return acc + 1
        }
        return acc
    }, 0)
    return (completedTask / stateLength) * 100
}