import { BrowserRouter, Route, Routes } from "react-router-dom"
import Home from "./component/home/Home"
import Lists from "./component/list/Lists"
import Header from "./component/header/Header"
import { useToast } from "./component/context/ToastContext"
import Toast from "./component/toast/Toast"

function App() {
  const { openError, toastStatus, handleCloseError } = useToast()
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/:boardId" element={<Lists />} />
      </Routes>
      <Toast
        open={openError}
        message={toastStatus.message}
        onClose={handleCloseError}
        severity={toastStatus.type}
      />
    </BrowserRouter>
  )
}

export default App
