const config = {
    apiKey: import.meta.env.VITE_API_KEY,
    token: import.meta.env.vITE_TOKEN,
    baseUrl: import.meta.env.vITE_BASE_URL
}

export default config